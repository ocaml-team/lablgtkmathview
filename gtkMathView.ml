(* Copyright (C) 2000-2005,
 *    Luca Padovani	      <lpadovan@cs.unibo.it>
 *    Claudio Sacerdoti Coen  <sacerdot@cs.unibo.it>
 *    Stefano Zacchiroli      <zacchiro@cs.unibo.it>
 *
 * This file is part of lablgtkmathview, the Ocaml binding for the
 * GtkMathView widget.
 * 
 * lablgtkmathview is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * lablgtkmathview is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lablgtkmathview; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * For details, send a mail to the authors.
 *)

open Gtk_mathview
open Gaux
open Gobject
open Gtk
open Tags
open GtkMathViewProps
open GtkBase

external _gtkmathview_init : unit -> unit = "ml_gtk_mathview_init"
let () = _gtkmathview_init ()

external add_configuration_path : string -> unit =
  "ml_gtk_math_view_add_configuration_path"

module MathView = struct
  include MathView_GMetaDOM
  external freeze : [>`mathview_gmetadom] obj -> unit =
   "ml_gtk_math_view_freeze"
  external thaw : [>`mathview_gmetadom] obj -> unit =
   "ml_gtk_math_view_thaw"
  external load_uri : [>`mathview_gmetadom] obj -> filename:string -> bool =
   "ml_gtk_math_view_load_uri"
  external load_root : [>`mathview_gmetadom] obj -> root:[> `Element] GdomeT.t -> bool =
   "ml_gtk_math_view_load_root"
  external unload : [>`mathview_gmetadom] obj -> unit =
   "ml_gtk_math_view_unload"
  external select :
   [>`mathview_gmetadom] obj -> [> `Element] GdomeT.t -> unit =
   "ml_gtk_math_view_select"
  external unselect :
   [>`mathview_gmetadom] obj -> [> `Element] GdomeT.t -> unit =
   "ml_gtk_math_view_unselect"
  external is_selected :
   [>`mathview_gmetadom] obj -> [> `Element] GdomeT.t -> bool =
   "ml_gtk_math_view_is_selected"
  external structure_changed :
   [>`mathview_gmetadom] obj -> [> `Element] GdomeT.t -> unit =
   "ml_gtk_math_view_structure_changed"
  external attribute_changed :
   [>`mathview_gmetadom] obj -> [> `Element] GdomeT.t -> name:TDOMString.t -> unit =
   "ml_gtk_math_view_attribute_changed"
  external get_bounding_box :
   [>`mathview_gmetadom] obj -> (int * int * int) =
   "ml_gtk_math_view_get_bounding_box"
  external get_element_at :
   [> `mathview_gmetadom] obj -> int -> int -> TElement.t option =
   "ml_gtk_math_view_get_element_at"
  external get_document :
   [> `mathview_gmetadom] obj -> TDocument.t option =
   "ml_gtk_math_view_get_document"
   (* beginning of prop-like methods *)
  external get_size : [>`mathview_gmetadom] obj -> int * int =
    "ml_gtk_math_view_get_size"
  external get_top : [>`mathview_gmetadom] obj -> (int * int) =
   "ml_gtk_math_view_get_top"
  external set_top : [>`mathview_gmetadom] obj -> int -> int -> unit =
   "ml_gtk_math_view_set_top"
  external set_adjustments : [>`mathview_gmetadom] obj -> Gtk.adjustment obj -> Gtk.adjustment obj -> unit =
   "ml_gtk_math_view_set_adjustments"
  external get_adjustments : [>`mathview_gmetadom] obj ->
    Gtk.adjustment obj * Gtk.adjustment obj =
      "ml_gtk_math_view_get_adjustments"
  external get_buffer : [>`mathview_gmetadom] obj -> Gdk.pixmap =
   "ml_gtk_math_view_get_buffer"
  external set_font_size : [>`mathview_gmetadom] obj -> int -> unit =
   "ml_gtk_math_view_set_font_size"
  external get_font_size : [>`mathview_gmetadom] obj -> int =
   "ml_gtk_math_view_get_font_size"
  external set_log_verbosity : [>`mathview_gmetadom] obj -> int -> unit =
   "ml_gtk_math_view_set_log_verbosity"
  external get_log_verbosity : [>`mathview_gmetadom] obj -> int =
   "ml_gtk_math_view_get_log_verbosity"
  external set_t1_opaque_mode : [>`mathview_gmetadom] obj -> bool -> unit =
   "ml_gtk_math_view_set_t1_opaque_mode"
  external get_t1_opaque_mode : [>`mathview_gmetadom] obj -> bool =
   "ml_gtk_math_view_get_t1_opaque_mode"
  external set_t1_anti_aliased_mode : [>`mathview_gmetadom] obj -> bool -> unit =
   "ml_gtk_math_view_set_t1_anti_aliased_mode"
  external get_t1_anti_aliased_mode : [>`mathview_gmetadom] obj -> bool =
   "ml_gtk_math_view_get_t1_anti_aliased_mode"
end
