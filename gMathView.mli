(* Copyright (C) 2000-2005,
 *    Luca Padovani	      <lpadovan@cs.unibo.it>
 *    Claudio Sacerdoti Coen  <sacerdot@cs.unibo.it>
 *    Stefano Zacchiroli      <zacchiro@cs.unibo.it>
 *
 * This file is part of lablgtkmathview, the Ocaml binding for the
 * GtkMathView widget.
 * 
 * lablgtkmathview is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * lablgtkmathview is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lablgtkmathview; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * For details, send a mail to the authors.
 *)

(** {1 Main interface to LablGtkMathView} *)

exception ErrorLoadingFile of string
exception ErrorWritingFile of string
exception ErrorLoadingDOM

(** {2 Standard classes} *)

(** Signals emitted by GtkMathView widgets *)
class math_view_signals :
  ([> `gtk | `mathview_gmetadom | `widget] as 'b) Gtk.obj ->
  object ('a)
    inherit GObj.widget_signals
    val after: bool
    val obj: 'b Gtk.obj
    method click :
      callback:(Gdome.element option * int * int * int -> unit) -> GtkSignal.id
    method element_over :
      callback:(Gdome.element option * int * int * int -> unit) -> GtkSignal.id
    method select_begin :
      callback:(Gdome.element option * int * int * int -> unit) -> GtkSignal.id
    method select_over :
      callback:(Gdome.element option * int * int * int -> unit) -> GtkSignal.id
    method select_end :
      callback:(Gdome.element option * int * int * int -> unit) -> GtkSignal.id
    method select_abort :
      callback:(unit -> unit) -> GtkSignal.id
  end

(** GtkMathView core methods *)
class math_view_skel :
  (Gtk_mathview.math_view Gtk.obj as 'a)->
  object
    inherit GObj.widget
    val obj : 'a
    method attribute_changed : Gdome.element -> name:Gdome.domString -> unit
    method event : GObj.event_ops
    method freeze : unit
    method get_adjustments : GData.adjustment * GData.adjustment
    method get_bounding_box : int * int * int
    method get_buffer : Gdk.pixmap
    method get_document : Gdome.document option
    method get_element_at : int -> int -> Gdome.element option  (* x, y *)
    method get_font_size : int
    method get_log_verbosity : int
    method get_size : int * int
    method get_t1_anti_aliased_mode : bool
    method get_t1_opaque_mode : bool
    method get_top : int * int
    method is_selected : Gdome.element -> bool
    method load_root : root:Gdome.element -> unit
    method load_uri : filename:string -> unit
    method select : Gdome.element -> unit
    method set_adjustments : GData.adjustment -> GData.adjustment -> unit
    method set_font_size : int -> unit
    method set_log_verbosity : int -> unit
    method set_top : int -> int -> unit
    method set_t1_anti_aliased_mode : bool -> unit
    method set_t1_opaque_mode : bool -> unit
    method structure_changed : Gdome.element -> unit
    method thaw : unit
    method unload : unit
    method unselect : Gdome.element -> unit
  end

(** GtkMathView standard class *)
class math_view :
  Gtk_mathview.math_view Gtk.obj ->
  object
    inherit math_view_skel
    method connect : math_view_signals
  end

(** {2 Constructors} *)

(** math_view constructor *)
val math_view :
  ?hadjustment:GData.adjustment ->
  ?vadjustment:GData.adjustment ->
  ?font_size:int ->
  ?log_verbosity:int ->
  ?width:int ->
  ?height:int ->
  ?packing:(GObj.widget -> unit) -> ?show:bool -> unit -> math_view

(** {2 Global configuration for all math_view instances} *)

  (** @param fname file name to be added to the list of configuration files read
   * at initialization time.
   * To be invoked before GTK initialization. *)
val add_configuration_path: string -> unit

