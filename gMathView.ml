(* Copyright (C) 2000-2005,
 *    Luca Padovani	      <lpadovan@cs.unibo.it>
 *    Claudio Sacerdoti Coen  <sacerdot@cs.unibo.it>
 *    Stefano Zacchiroli      <zacchiro@cs.unibo.it>
 *
 * This file is part of lablgtkmathview, the Ocaml binding for the
 * GtkMathView widget.
 * 
 * lablgtkmathview is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * lablgtkmathview is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lablgtkmathview; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * For details, send a mail to the authors.
 *)

open Gaux
open Gtk_mathview
open Gobject
open Gtk
open GtkBase
open GtkMathView
open OgtkMathViewProps
open GObj

exception ErrorLoadingFile of string;;
exception ErrorWritingFile of string;;
exception ErrorLoadingDOM;;

let option_element_of_option =
 function
    None -> None
  | Some v -> Some (new Gdome.element v)

let option_document_of_option =
 function
    None -> None
  | Some v -> Some (new Gdome.document v)

class math_view_skel obj = object
 inherit GObj.widget (obj : Gtk_mathview.math_view obj)
 method event = new GObj.event_ops obj
 method freeze = MathView.freeze obj
 method thaw = MathView.thaw obj
 method load_uri ~filename =
  if not (MathView.load_uri obj ~filename) then raise (ErrorLoadingFile filename)
 method load_root ~root =
  if not (MathView.load_root obj ~root:((root : Gdome.element)#as_Element)) then
   raise ErrorLoadingDOM
 method unload = MathView.unload obj
 method select element = MathView.select obj ((element : Gdome.element)#as_Element)
 method unselect element = MathView.unselect obj ((element : Gdome.element)#as_Element)
 method is_selected element = MathView.is_selected obj ((element : Gdome.element)#as_Element)
 method get_element_at x y = option_element_of_option (MathView.get_element_at obj x y)
 method get_document = option_document_of_option (MathView.get_document obj)
 method structure_changed element = MathView.structure_changed obj ((element : Gdome.element)#as_Element)
 method attribute_changed element ~name = MathView.attribute_changed obj ((element : Gdome.element)#as_Element) ((name : Gdome.domString)#as_DOMString)
 method get_bounding_box = MathView.get_bounding_box obj
 method get_size = MathView.get_size obj
 method get_top = MathView.get_top obj
 method set_top x y  = MathView.set_top obj x y
 method set_adjustments adj1 adj2 = MathView.set_adjustments obj (GData.as_adjustment adj1) (GData.as_adjustment adj2)
 method get_adjustments =
   let hadj, vadj = MathView.get_adjustments obj in
   new GData.adjustment hadj, new GData.adjustment vadj
 method get_buffer = MathView.get_buffer obj
 method set_font_size = MathView.set_font_size obj
 method get_font_size = MathView.get_font_size obj
 method set_log_verbosity = MathView.set_log_verbosity obj
 method get_log_verbosity = MathView.get_log_verbosity obj
 method set_t1_opaque_mode = MathView.set_t1_opaque_mode obj
 method get_t1_opaque_mode = MathView.get_t1_opaque_mode obj
 method set_t1_anti_aliased_mode = MathView.set_t1_anti_aliased_mode obj
 method get_t1_anti_aliased_mode = MathView.get_t1_anti_aliased_mode obj
end

class math_view_signals obj = object
  inherit GObj.widget_signals_impl obj
  inherit math_view__g_meta_dom_sigs
end

class math_view obj = object
  inherit math_view_skel (obj : Gtk_mathview.math_view obj)
  method connect = new math_view_signals obj
end

let math_view ?hadjustment ?vadjustment ?font_size ?log_verbosity =
  GtkBase.Widget.size_params ~cont:(
  OgtkMathViewProps.pack_return
    (fun p -> OgtkMathViewProps.set_params (new math_view (MathView.create p)) ~font_size ~log_verbosity)) []

let add_configuration_path = GtkMathView.add_configuration_path

