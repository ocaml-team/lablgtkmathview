/* Copyright (C) 2000-2005,
 *    Luca Padovani	      <lpadovan@cs.unibo.it>
 *    Claudio Sacerdoti Coen  <sacerdot@cs.unibo.it>
 *    Stefano Zacchiroli      <zacchiro@cs.unibo.it>
 *
 * This file is part of lablgtkmathview, the Ocaml binding for the
 * GtkMathView widget.
 * 
 * lablgtkmathview is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * lablgtkmathview is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lablgtkmathview; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * For details, send a mail to the authors.
 */

#include <assert.h>

#include <gtk/gtkmathview_gmetadom.h>
#include <gtk/gtk.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/custom.h>
#include <caml/callback.h>

#include <wrappers.h>
#include <ml_glib.h>
#include <ml_gdk.h>
#include <ml_gtk.h>
#include <ml_gobject.h>
#include <ml_gdkpixbuf.h>
#include <ml_pango.h>
#include <gtk_tags.h>
#include <gdk_tags.h>

#include <mlgdomevalue.h>

/* Init all */

CAMLprim value ml_gtk_mathview_init(value unit)
{
    /* Since these are declared const, must force gcc to call them! */
    GType t = gtk_math_view_get_type();
    return Val_GType(t);
}

#define GtkMathView_val(val) check_cast(GTK_MATH_VIEW,val)

//#####################################
//
//#define FontManagerId_val(val) Int_val(val)
//#define Val_FontManagerId(val) Val_int(val)
//
///* As ML_1, but the result is optional */
//#define OML_1(cname, conv1, conv) \
//value ml_##cname (value arg1) { return Val_option_ptr((cname (conv1 (arg1))),conv); }
///* As ML_3, but the result is optional */
#define OML_3(cname, conv1, conv2, conv3, conv) \
value ml_##cname (value arg1, value arg2, value arg3) { return Val_option_ptr((cname (conv1 (arg1), conv2 (arg2), conv3 (arg3))),conv); }
///* As ML_2, but the second argument is optional */
//#define ML_2O(cname, conv1, conv2, conv) \
//value ml_##cname (value arg1, value arg2) \
//{ return conv (cname (conv1(arg1), ptr_val_option(arg2,conv2))); }

value Val_Element_ref(GdomeElement* elem)
{
  if (elem != NULL)
    {
      GdomeException exc = 0;
      gdome_el_ref(elem, &exc);
      g_assert(exc == 0);
    }
  return Val_Element(elem);
}

ML_2 (gtk_math_view_structure_changed, GtkMathView_val, Element_val, Unit)
ML_3 (gtk_math_view_attribute_changed, GtkMathView_val, Element_val, DOMString_val, Unit)
/* OML_3 (gtk_math_view_get_element_at, GtkMathView_val, Int_val, Int_val, Val_Element) */
ML_1 (gtk_math_view_freeze, GtkMathView_val, Unit)
ML_1 (gtk_math_view_thaw, GtkMathView_val, Unit)
ML_2 (gtk_math_view_load_uri, GtkMathView_val, String_val, Val_bool)
ML_2 (gtk_math_view_load_root, GtkMathView_val, Element_val, Val_bool)
ML_1 (gtk_math_view_unload, GtkMathView_val, Unit)
ML_2 (gtk_math_view_select, GtkMathView_val, Element_val, Unit)
ML_2 (gtk_math_view_unselect, GtkMathView_val, Element_val, Unit)
ML_2 (gtk_math_view_is_selected, GtkMathView_val, Element_val, Val_bool)
/*
ML_2 (gtk_math_view_new,GtkAdjustment_val, GtkAdjustment_val, Val_GtkWidget_sink)
ML_1 (gtk_math_view_get_width, GtkMathView_val, Val_int)
ML_1 (gtk_math_view_get_height, GtkMathView_val, Val_int)
*/
ML_3 (gtk_math_view_set_top, GtkMathView_val, Int_val, Int_val, Unit)
ML_3 (gtk_math_view_set_adjustments, GtkMathView_val, GtkAdjustment_val, GtkAdjustment_val, Unit)
/*
ML_1 (gtk_math_view_get_hadjustment, GtkMathView_val, Val_GtkWidget)
ML_1 (gtk_math_view_get_vadjustment, GtkMathView_val, Val_GtkWidget)
*/
ML_1 (gtk_math_view_get_buffer, GtkMathView_val, Val_GdkPixmap)
ML_2 (gtk_math_view_set_font_size, GtkMathView_val, Int_val, Unit)
ML_1 (gtk_math_view_get_font_size, GtkMathView_val, Val_int)
ML_2 (gtk_math_view_set_log_verbosity, GtkMathView_val, Int_val, Unit)
ML_1 (gtk_math_view_get_log_verbosity, GtkMathView_val, Val_int)
ML_2 (gtk_math_view_set_t1_opaque_mode, GtkMathView_val, Bool_val, Unit)
ML_1 (gtk_math_view_get_t1_opaque_mode, GtkMathView_val, Val_bool)
ML_2 (gtk_math_view_set_t1_anti_aliased_mode, GtkMathView_val, Bool_val, Unit)
ML_1 (gtk_math_view_get_t1_anti_aliased_mode, GtkMathView_val, Val_bool)
ML_1 (gtk_math_view_add_configuration_path, String_val, Unit)

value ml_gtk_math_view_get_element_at (value arg1, value arg2, value arg3)
{
   CAMLparam3(arg1, arg2, arg3);
   CAMLlocal1 (result);
   GdomeElement* el;
   if (gtk_math_view_get_element_at(GtkMathView_val (arg1), Int_val(arg2), Int_val(arg3), &el, NULL, NULL))
     result = Val_option_ptr(el, Val_Element);
   else
     result = Val_unit;
   CAMLreturn (result);
}

value ml_gtk_math_view_get_document (value arg1)
{
   CAMLparam1(arg1);
   CAMLlocal1(result);
   GdomeDocument* doc = gtk_math_view_get_document(GtkMathView_val (arg1));
   if (doc == NULL)
	   result = Val_unit;
   else
	   result = Val_option_ptr(doc, Val_Document);
   CAMLreturn (result);
}

value ml_gtk_math_view_get_adjustments(value arg1)
{
   CAMLparam1(arg1);
   CAMLlocal1(result);
   GtkAdjustment* hadj;
   GtkAdjustment* vadj;
   gtk_math_view_get_adjustments(GtkMathView_val (arg1), &hadj, &vadj);
   result = alloc(2, 0);
   Store_field(result, 0, Val_GtkWidget(hadj));
   Store_field(result, 1, Val_GtkWidget(vadj));
   CAMLreturn(result);
}

value ml_gtk_math_view_get_size (value arg1)
{
   CAMLparam1(arg1);
   CAMLlocal1(result);
   int width, height;
   gtk_math_view_get_size(GtkMathView_val (arg1), &width, &height);
   result = alloc(1, 0);
   Store_field(result, 0, Val_int(width));
   Store_field(result, 1, Val_int(height));
   CAMLreturn (result);
}

value ml_gtk_math_view_get_bounding_box (value arg1)
{
   CAMLparam1(arg1);
   CAMLlocal1(result);
   int width, height, depth;
   GtkMathViewBoundingBox gbox;
   gtk_math_view_get_bounding_box(GtkMathView_val (arg1), &gbox);
   result = alloc(3, 0);
   Store_field(result, 0, Val_int(gbox.width));
   Store_field(result, 1, Val_int(gbox.height));
   Store_field(result, 2, Val_int(gbox.depth));
   CAMLreturn (result);
}

value ml_gtk_math_view_get_top (value arg1)
{
   CAMLparam1(arg1);
   CAMLlocal1(result);
   int x, y;
   gtk_math_view_get_top(GtkMathView_val (arg1), &x, &y);
   result = alloc(2, 0);
   Store_field(result, 0, Val_int(x));
   Store_field(result, 1, Val_int(y));
   CAMLreturn (result);
}

/*
value ml_gtk_math_view_get_element_coords (value arg1, value arg2)
{
  CAMLparam2(arg1, arg2);
  CAMLlocal1 (result);
  int x, y;
  gtk_math_view_get_element_coords(GtkMathView_val (arg1), Element_val(arg2), &x, &y);
  result = alloc(2, 0);
  Store_field(result, 0, Val_int(x));
  Store_field(result, 1, Val_int(y));
  CAMLreturn (result);
}
*/

value ml_gtk_math_view_gdome_element_of_boxed_option (value arg1)
{
   CAMLparam1(arg1);

   GdomeException exc = 0;
   GdomeElement* nr = NULL;
   CAMLlocal1 (res);

   if (arg1==Val_int(0)) {
      assert(0);
   } else {
      nr = (GdomeElement*) Field(Field(arg1,0),1);
   }

   res = Val_Element_ref(nr);
   if (res==Val_int(0)) {
      assert(0);
   }

   CAMLreturn(res);
}

value ml_gtk_math_view_gdome_element_option_of_boxed_option (value arg1)
{
   CAMLparam1(arg1);

   GdomeElement* nr;
   CAMLlocal1 (valnr);
   CAMLlocal1 (res);

   if (arg1==Val_int(0)) {
      res=Val_unit;
   } else {
      GdomeException exc = 0;
      GdomeElement* elem = (GdomeElement*) Field(Field(arg1,0),1);
      assert(elem != NULL);
      res = Val_option_ptr(elem, Val_Element_ref);
   }

   CAMLreturn(res);
}

value ml_gtk_math_view_model_event_of_boxed_option (value arg1)
{
   CAMLparam1(arg1);
   GdomeElement* nr;
   CAMLlocal1 (valnr);
   CAMLlocal1 (res);

   assert(arg1 != Val_int(0));
   GtkMathViewModelEvent* event = (GtkMathViewModelEvent*) Field(Field(arg1,0),1);
   res = alloc(4, 0);
   Store_field(res, 0, Val_option_ptr(event->id, Val_Element_ref));
   Store_field(res, 1, Val_int(event->x));
   Store_field(res, 2, Val_int(event->y));
   Store_field(res, 3, Val_int(event->state));

   CAMLreturn(res);
}

