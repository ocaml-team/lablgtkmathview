(* Copyright (C) 2000-2005,
 *    Luca Padovani	      <lpadovan@cs.unibo.it>
 *    Claudio Sacerdoti Coen  <sacerdot@cs.unibo.it>
 *    Stefano Zacchiroli      <zacchiro@cs.unibo.it>
 *
 * This file is part of lablgtkmathview, the Ocaml binding for the
 * GtkMathView widget.
 * 
 * lablgtkmathview is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * lablgtkmathview is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lablgtkmathview; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * For details, send a mail to the authors.
 *)

(** {1 Selection-enabled GtkMathView classes} *)

(** {2 Single-selection} *)

(** signals *)
class single_selection_math_view_signals :
  ([> `gtk | `mathview_gmetadom | `widget] as 'b) Gtk.obj ->
  ((Gdome.element option -> unit) -> unit) ->
  object
    inherit GMathView.math_view_signals
    method selection_changed : (Gdome.element_of_node option -> unit) -> unit
  end

(** main class *)
class single_selection_math_view :
  Gtk_mathview.math_view Gtk.obj ->
  object
    inherit GMathView.math_view_skel
    method connect : single_selection_math_view_signals
    method get_selection : Gdome.element option
    method set_selection : Gdome.element option -> unit
    method action_toggle : Gdome.element -> bool
  end

(** single_selection_math_view constructor *)
val single_selection_math_view :
  ?hadjustment:GData.adjustment ->
  ?vadjustment:GData.adjustment ->
  ?font_size:int ->
  ?log_verbosity:int ->
  ?width:int ->
  ?height:int ->
  ?packing:(GObj.widget -> unit) ->
  ?show:bool ->
  unit ->
    single_selection_math_view

(** {2 Multiple-selection} *)

(** main class, extends single_selection_math_view *)
class multi_selection_math_view :
  Gtk_mathview.math_view Gtk.obj ->
  object
    inherit single_selection_math_view
    method remove_selection : Gdome.element -> unit
    method remove_selections : unit
    method add_selection : Gdome.element -> unit
    method get_selections : Gdome.element list
  end

(** multi_selection_math_view constructor *)
val multi_selection_math_view :
  ?hadjustment:GData.adjustment ->
  ?vadjustment:GData.adjustment ->
  ?font_size:int ->
  ?log_verbosity:int ->
  ?width:int ->
  ?height:int ->
  ?packing:(GObj.widget -> unit) ->
  ?show:bool ->
  unit ->
    multi_selection_math_view

